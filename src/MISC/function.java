/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MISC;

import Alert.controller.AlertBoxController;
import Confirm.controller.ConfirmBoxController;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;

/**
 *
 * @author Milind Patel
 */
public class function {

    public void showAlert(String msg, String color, Window owner) {
        try {
            AlertBoxController alert = new AlertBoxController().returnController();
            alert.setLblTxt(msg);
            alert.setLblColor(color);
            alert.setParentWindow(owner);
            alert.setUpStage();
            alert.showAndWaitAlert();
//            alert.setParentWindow((Stage)addBtn.getScene().getWindow());
////            alert.setUpStage();
//            alert.showAndWaitAlert();
        } catch (Exception ex) {

        }
    }

    public boolean showConfirmBox(String msg, String color, Window owner) {

        try {
            ConfirmBoxController confirm = new ConfirmBoxController().returnController();
            confirm.setLblTxt(msg);
            confirm.setLblColor(color);
            confirm.setParentWindow(owner);
            confirm.setUpStage();
            confirm.showAndWaitConfirmBox();
//            System.out.println("Jordar baki ho");
            return confirm.getResult();
//            alert.setParentWindow((Stage)addBtn.getScene().getWindow());
////            alert.setUpStage();
//            alert.showAndWaitAlert();
        } catch (Exception ex) {

        }
        return false;
    }

    public boolean creteFolder(String folderName) {
        File theDir = new File(folderName);

// if the directory does not exist, create it
        if (!theDir.exists()) {
            System.out.println("creating directory: " + folderName);
            boolean result = false;

            try {
                theDir.mkdirs();
                result = true;
            } catch (SecurityException se) {
                //handle it
            }
            if (result) {
                return true;
            }
        }
        return false;
    }

    public void openPopUp(String path, Window owner, String title) {
        try {
            Stage stage;

            Parent root;
            stage = new Stage();
            root = FXMLLoader.load(getClass().getResource(path));
//            root = FXMLLoader.load(getClass().getResource("/view/view/configrate.fxml"));
            stage.setScene(new Scene(root));
            stage.setTitle(title);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initOwner(owner);
            stage.initStyle(StageStyle.UTILITY);
            stage.setResizable(false);
            stage.showAndWait();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
