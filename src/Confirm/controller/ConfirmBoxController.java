/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Confirm.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import javafx.stage.WindowEvent;

/**
 * FXML Controller class
 *
 * @author Milind Patel
 */
public class ConfirmBoxController implements Initializable {

    /**
     * Initializes the controller class.
     */
    
    String lblTxt = "New Warning Please click On Ok";
    String lblColor = "red";
    boolean result = false;
    Stage stage;
    Window parentWindow;
    Parent root;

    public String getLblTxt() {
        return lblTxt;
    }

    public void setLblTxt(String lblTxt) {
         lblMsg.setText(lblTxt);
        this.lblTxt = lblTxt;
    }

    public boolean getResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getLblColor() {
        return lblColor;
    }

    public void setLblColor(String lblColor) {
        lblMsg.setTextFill(Color.web(lblColor));
        this.lblColor = lblColor;
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public Window getParentWindow() {
        return parentWindow;
    }

    public void setParentWindow(Window parentWindow) {
        this.parentWindow = parentWindow;
    }

    public Parent getRoot() {
        return root;
    }

    public void setRoot(Parent root) {
        this.root = root;
    }
    @FXML 
    private Button yesBtn;
    
    @FXML
    private Button noBtn;
    
    @FXML
    private  Label lblMsg;
    
    @FXML 
    public void yesBtnClicked(ActionEvent e){
        result = true;
        stage.close();
        
    }
    
    @FXML
    public void noBtnClicked(ActionEvent e){
    result = false;
    stage.close();
    }
    
    public void showAndWaitConfirmBox(){
        stage.showAndWait();
        
    }
    public void showConfirm(){
        stage.show();
        
    }
    
     public void setUpStage(){
        stage = new Stage();
        Scene alertScene = new Scene(root);
        stage.setScene(alertScene);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initStyle(StageStyle.UTILITY);
        stage.initOwner(parentWindow);
    }
     public ConfirmBoxController returnController(){
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/Confirm/fxml/ConfirmBox.fxml"));
            Parent root = loader.load();
            ConfirmBoxController confirm = (ConfirmBoxController) loader.getController();
            confirm.setRoot(root);
            return confirm;
            
        } catch (IOException ex) {
           ex.printStackTrace();
           return null;
        }
     
     }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        lblMsg.setText(lblTxt);
    }    
    
   
    
}
