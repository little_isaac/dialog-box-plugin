/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Alert.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import javafx.stage.WindowEvent;

/**
 * FXML Controller class
 *
 * @author Milind Patel
 */
public class AlertBoxController implements Initializable {

    /**
     * Initializes the controller class.
     */
    
    
    String lblTxt = "New Warning Please click On Ok";
    String lblColor = "red";

    @FXML
    Label lblMsg;

    @FXML
    Button okBtn;

    Stage stage;
    Window parentWindow;
    Parent root;

    public void setRoot(Parent root) {
        this.root = root;
    }

    public String getLblColor() {
        return lblColor;
    }

    public void setLblColor(String lblColor) {
        lblMsg.setTextFill(Color.web(lblColor));
        this.lblColor = lblColor;
    }

    public Parent getRoot() {
        return root;
    }

    public String getLblTxt() {
        return lblTxt;
    }

    public void setLblTxt(String lblTxt) {
        lblMsg.setText(lblTxt);
        this.lblTxt = lblTxt;
    }

    public Window getParentWindow() {
        return parentWindow;
    }

    public void setParentWindow(Window parentWindow) {
        this.parentWindow = parentWindow;
    }

    public AlertBoxController returnController() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/Alert/fxml/AlertBox.fxml"));
            Parent root1 = loader.load();
            AlertBoxController alert = (AlertBoxController) loader.getController();

            alert.setRoot(root1);
            return alert;
        } catch (IOException ex) {
           ex.printStackTrace();
           return null;
        }
    }

    public void setUpStage() {
        stage = new Stage();
        Scene alertScene = new Scene(root);
        stage.setScene(alertScene);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.initStyle(StageStyle.UTILITY);

        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {

            @Override
            public void handle(WindowEvent event) {

            }
        });
        stage.initOwner(parentWindow);
    }

    @FXML
    public void okBtnClicked(ActionEvent e) {
        stage = (Stage) okBtn.getScene().getWindow();
        stage.close();
    }

    public void showAlert() {
        stage.show();

    }

    public void showAndWaitAlert() {
        stage.showAndWait();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
//       setUpStage();
        lblMsg.setText(lblTxt);

    }

}
